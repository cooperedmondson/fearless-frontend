function showAlert() {
    return `<div class="alert alert-warning alert-dismissible fade show" role="alert">
    <strong>WOW YOU BROKE IT!</strong> You should go home.
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
  </div>`;
}






function createCard(title, description, pictureUrl, startDate, endDate, locationName) {
    return `
    <div class="col">
        <div class="shadow-lg p-3 mb-5 bg-body rounded">
            <div class="card">
                <img src="${pictureUrl}" class="card-img-top">
                    <div class="card-body">
                    <h5 class="card-title">${title}</h5>
                    <h6 class="card-subtitle mb-2 text-muted">${locationName}</h6>
                    <p class="card-text">${description}</p>
                    </div>
                </div>
            <div class="card-footer text-muted">${startDate} - ${endDate}</div>
        </div>
    </div>
    `;
  }

  

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
  
    try {
        const response = await fetch(url);
    
        if (!response.ok) {
          const errorThrower = showAlert()
          const errorWOW = document.querySelector('.row');
          errorWOW.innerHTML += errorThrower;;
          console.log(`The site is broken. Error code: ${response.status}`)

        } else {
          const data = await response.json();
    
          for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
              const details = await detailResponse.json();
              const title = details.conference.name;
              const startDate = new Date(details.conference.starts).toDateString();
              const endDate = new Date(details.conference.ends).toDateString();
              const description = details.conference.description;
              const locationName = details.conference.location.name;
              const pictureUrl = details.conference.location.picture_url;
              const html = createCard(
                title,
                description,
                pictureUrl,
                startDate, 
                endDate,
                locationName,
                );
              const column = document.querySelector('.row');
              column.innerHTML += html;;
            }
          }
    
        }
      } catch (e) {
        console.log(`THE SITE BROKE! ERROR CODE: ${e}`);
        
    }
  
  });
