window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/states/';

    const response = await fetch(url);

    if(response.ok) {
        const stateData = await response.json();
        const stateTag = document.getElementById('state')
        // console.log(stateData);
        for (const state of stateData.states ){
            const stateName = Object.keys(state)[0]
            const stateAbv = Object.values(state)[0]
            const option = document.createElement('option')
            option.innerHTML = stateName;
            option.value = stateAbv;
            stateTag.appendChild(option)





        }

        const formTag = document.getElementById('create-location-form');
        formTag.addEventListener('submit', async event => {
            event.preventDefault();
            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));

            const locationUrl = 'http://localhost:8000/api/locations/';
            const fetchConfig = {
                method: 'post',
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            const response = await fetch(locationUrl, fetchConfig);
            if (response.ok) {
                formTag.reset();
                const newLocation = await response.json();
                console.log(newLocation)
            }
        })


    }
});
        
    

    